"""
Description
"""

import os

from tornado.options import define, options

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"


ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def path(*a):
    return os.path.join(ROOT, *a)


LOGS_PATH = path('var', 'logs')

define("port", default=80, help="run on the given port", type=int)
define("debug", default=False, help="debug mode")
define("log_file_prefix", default=path(LOGS_PATH, 'app.log'), help="path to log file")
define("log_to_stderr", default=False, help="log to stderr")


if not os.path.exists(LOGS_PATH):
    os.makedirs(LOGS_PATH)


TORNADO = {
    'debug': options.debug,
    'autoreload': False
}
