build-base:
	docker build -t github-wrapper-api-base:latest -f docker/prod/Dockerfile .

build-prod:
	docker build -t github-wrapper-api:latest -f docker/prod/Dockerfile .

build: build-base
	docker build -t github-wrapper-api-dev:latest -f docker/dev/Dockerfile .

up-prod:
	docker-compose -f docker/prod/docker-compose.yml up -d

up:
	docker-compose -f docker/dev/docker-compose.yml up -d

down-prod:
	docker-compose -f docker/prod/docker-compose.yml down

down:
	docker-compose -f docker/dev/docker-compose.yml down

stop-prod:
	docker-compose -f docker/prod/docker-compose.yml stop

stop:
	docker-compose -f docker/dev/docker-compose.yml stop

logs-prod:
	docker-compose -f docker/prod/docker-compose.yml logs

logs:
	docker-compose -f docker/dev/docker-compose.yml logs

all: build-prod up-prod
