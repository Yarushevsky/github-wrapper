"""
Description
"""

from handlers.api.v1.repos.pulls import PullsHandler, PullCommentsHandler

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"


urls = [
    (r"/api/v1/repos/(?P<owner>[a-zA-Z0-9]+)/(?P<repo>[a-zA-Z0-9]+)/pulls$", PullsHandler),
    (r"/api/v1/repos/(?P<owner>[a-zA-Z0-9]+)/(?P<repo>[a-zA-Z0-9]+)/pulls/(?P<number>[0-9]+)/comments$",
     PullCommentsHandler)
]
