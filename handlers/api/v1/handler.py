"""
Description
"""

from tornado import escape
from tornado.log import app_log
from tornado.web import RequestHandler

from handlers.api.v1.response import Error500Response, Error403Response, Error405Response, Error404Response, \
    SuccessResponse, ErrorResponse
from helper import wrap_handler_error, HandlerError

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"


class BaseAPIHandler(RequestHandler):
    SUPPORTED_METHODS = ("GET", "POST", "DELETE", "PUT")

    ERROR_RESPONSE = {
        404: Error404Response,
        405: Error405Response,
        500: Error500Response,
        403: Error403Response
    }

    SUCCESS_CODE_RESPONSE = [200, 201]
    ERROR_CODE_RESPONSE = [401, 403, 404, 405, 409, 500]

    def __init__(self, *args, **kwargs):
        """
        Constructor

        :param args:
        :param kwargs:
        :return:
        """
        super(BaseAPIHandler, self).__init__(*args, **kwargs)
        self.json_data = None

    @wrap_handler_error
    def get(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        raise HandlerError(code=405)

    @wrap_handler_error
    def post(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        raise HandlerError(code=405)

    @wrap_handler_error
    def put(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        raise HandlerError(code=405)

    @wrap_handler_error
    def delete(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        raise HandlerError(code=405)

    def data_received(self, chunk):
        pass

    def parse_json(self, data):
        """
        Get and parse json string from request

        :return:
        """
        try:
            if self.request.headers['Content-Type'].startswith("application/json") and self.request.body:
                self.json_data = escape.json_decode(data)
        except (ValueError, KeyError):
            app_log.critical(self.request.body)
            app_log.critical(str(self.request.headers))
            app_log.critical(self.request.headers.get('Content-Type', ''))
            self.send_response(code=409, error_code=2001, message=_("Unable to parse JSON"))

    def set_default_headers(self):
        """
        Set default headers of response

        :return:
        """
        self.set_header('Content-Type', 'application/json')
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers',
                        "Content-Type, Access-Control-Allow-Headers, Access-Control-Allow-Methods, \
                        X-Requested-With")
        self.set_header('Access-Control-Allow-Methods', "POST, PUT, DELETE, GET")

    def send_response(self, code, *args, **kwargs):
        """
        Formatting response

        :param code: response status code
        :param args:
        :param kwargs: response information
        :return:
        """
        self.set_status(code)
        response = SuccessResponse(code=code, **kwargs) if code in self.SUCCESS_CODE_RESPONSE else \
            ErrorResponse(code=code, locale=self.locale, **kwargs)
        self.write(response.as_json())
        self.finish()

    def write_error(self, status_code, **kwargs):
        """
        Formatting standards errors

        :param status_code: response status code
        :param kwargs:
        :return:
        """
        self.set_status(status_code)
        response = self.ERROR_RESPONSE[status_code]()
        self.write(response.as_json())
        self.finish()

    def query_arguments_as_json(self):
        """
        Get query arguments as json

        :return:
        """
        return {key: (
            lambda value: escape.to_basestring(value[0]) if len(value) == 1 else [escape.to_basestring(x) for x in
                                                                                  value])(
                value) for (key, value) in self.request.query_arguments.items()}


class BaseErrorHandler(BaseAPIHandler):
    """
    Default handler gonna to be used in case of 404 error
    """
    @wrap_handler_error
    def get(self, *args, **kwargs):
        raise HandlerError(code=404)

    @wrap_handler_error
    def post(self, *args, **kwargs):
        raise HandlerError(code=404)

    @wrap_handler_error
    def put(self, *args, **kwargs):
        raise HandlerError(code=404)

    @wrap_handler_error
    def delete(self, *args, **kwargs):
        raise HandlerError(code=404)
