"""
Description
"""

from tornado import httpclient
from tornado import escape

from handlers.api.v1.handler import BaseAPIHandler
from helper import wrap_handler_error, HandlerError
from libs.validation import json_validate_get_params

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"


class PullsHandler(BaseAPIHandler):

    pulls_url = "https://api.github.com/repos/{owner}/{repo}/pulls?per_page={per_page}&page={page}"
    users_url = "https://api.github.com/users/{login}"

    def __init__(self, *args, **kwargs):
        super(PullsHandler, self).__init__(*args, **kwargs)
        self.http_client = httpclient.AsyncHTTPClient()

    @json_validate_get_params({
        'type': 'object',
        'properties': {
            'per_page': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 100
            },
            'page': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 100
            }
        }
    }, {
        'per_page': int,
        'page': int
    })
    @wrap_handler_error
    async def get(self, owner, repo, *args, **kwargs):
        """

        :param owner:
        :param repo:
        :param args:
        :param kwargs:
        :return:
        """

        query_args = self.query_arguments_as_json()

        decoded_response = await self._get_pulls(owner, repo, **query_args)

        data = []
        for body in decoded_response:
            decoded_user_response = await self._get_user(body["user"]["login"])
            data.append({
                "id": body["id"],
                "number": body["number"],
                "title": body["title"],
                "created_at": body["created_at"],
                "updated_at": body["updated_at"],
                "requested_reviewers": body["requested_reviewers"],
                "user": {
                    "login": body["user"]["login"],
                    "avatar_url": body["user"]["avatar_url"],
                    "email": decoded_user_response["email"]
                }
            })

        self.send_response(code=200, data=data)

    async def _get_pulls(self, owner, repo, per_page=10, page=1):
        """

        :param owner:
        :param repos:
        :return:
        """
        request = httpclient.HTTPRequest(
            self.pulls_url.format(owner=owner, repo=repo, per_page=per_page, page=page),
            headers={
                'User-Agent': 'Chrome'
            }
        )
        response = await self.http_client.fetch(request)
        if response.code != 200:
            raise HandlerError(code=404)

        return escape.json_decode(response.body)

    async def _get_user(self, login):
        """

        :param login:
        :return:
        """
        request = httpclient.HTTPRequest(
            self.users_url.format(login=login),
            headers={
                'User-Agent': 'App'
            }
        )

        response = await self.http_client.fetch(request)
        if response.code != 200:
            raise HandlerError(code=404)

        return escape.json_decode(response.body)


class PullCommentsHandler(BaseAPIHandler):

    comments_url = "https://api.github.com/repos/{owner}/{repo}/pulls/{number}/comments?per_page={per_page}&page={page}"

    def __init__(self, *args, **kwargs):
        super(PullCommentsHandler, self).__init__(*args, **kwargs)
        self.http_client = httpclient.AsyncHTTPClient()

    @json_validate_get_params({
        'type': 'object',
        'properties': {
            'per_page': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 100
            },
            'page': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 100
            }
        }
    }, {
        'per_page': int,
        'page': int
    })
    @wrap_handler_error
    async def get(self, owner, repo, number, *args, **kwargs):
        """

        :param owner:
        :param repo:
        :param number:
        :param args:
        :param kwargs:
        :return:
        """

        query_args = self.query_arguments_as_json()

        decoded_response = await self._get_comments(owner, repo, number, **query_args)

        data = [
            {
                "login": body["user"]["login"],
                "body": body["body"]
            } for body in decoded_response
        ]

        self.send_response(code=200, data=data)

    async def _get_comments(self, owner, repo, number, per_page=10, page=1):
        """

        :param owner:
        :param repo:
        :param number:
        :return:
        """
        request = httpclient.HTTPRequest(
            self.comments_url.format(owner=owner, repo=repo, number=number, per_page=per_page, page=page),
            headers={
                'User-Agent': 'App'
            }
        )

        response = await self.http_client.fetch(request)
        if response.code != 200:
            raise HandlerError(code=404)

        return escape.json_decode(response.body)
