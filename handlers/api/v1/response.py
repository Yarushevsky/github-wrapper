"""
Description
"""

import json

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"

API_VERSION = 1


class BaseAPIResponse(object):
    def __init__(self, code, status, message=None, *args, **kwargs):
        """

        :param code: integer HTTP Response Code
        :param status: integer system status code: 1 - success, 0 - error
        :param message: string response message
        :return: None
        """
        self.version = API_VERSION
        self.code = code
        self.status = status
        self.message = message

    def as_json(self):
        """
        Returns response as a dictionary
        :return: dict Dictionary representation of the response object
        """
        return self.__dict__

    def as_json_str(self):
        """
        Returns response as json string
        :return: string
        """
        return json.dumps(self.as_json())


class SuccessResponse(BaseAPIResponse):
    def __init__(self, data=None, *args, **kwargs):
        """

        :param data: dict data of response
        :param args:
        :param kwargs:
        :return:
        """
        kwargs['status'] = 1
        super(SuccessResponse, self).__init__(*args, **kwargs)
        self.data = data


class ErrorResponse(BaseAPIResponse):
    def __init__(self, errors=None, error_code=None, *args, **kwargs):
        """

        :param errors: list of errors
        :param error_code: code of error
        :param args:
        :param kwargs:
        :return:
        """
        if errors is None:
            errors = []
        kwargs['status'] = 0
        super(ErrorResponse, self).__init__(*args, **kwargs)
        self.errors = errors
        self.error_code = error_code


class Error405Response(ErrorResponse):
    def __init__(self, *args, **kwargs):
        kwargs['message'] = "Method Not Allowed"
        kwargs['code'] = 405
        super(Error405Response, self).__init__(*args, **kwargs)


class Error404Response(ErrorResponse):
    def __init__(self, *args, **kwargs):
        kwargs['message'] = "Not Found"
        kwargs['code'] = 404
        super(Error404Response, self).__init__(*args, **kwargs)


class Error500Response(ErrorResponse):
    def __init__(self, *args, **kwargs):
        kwargs['message'] = "Internal server error"
        kwargs['code'] = 500
        super(Error500Response, self).__init__(*args, **kwargs)


class Error403Response(ErrorResponse):
    def __init__(self, *args, **kwargs):
        kwargs['message'] = "Forbidden"
        kwargs['code'] = 403
        super(Error403Response, self).__init__(*args, **kwargs)
