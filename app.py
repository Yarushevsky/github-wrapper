"""
Description
"""

import settings

import tornado.options
import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.options import options

from handlers.api.v1.handler import BaseErrorHandler
from handlers.api.v1.urls import urls

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"


tornado.options.parse_command_line()


def main():

    settings.TORNADO['default_handler_class'] = BaseErrorHandler

    app = tornado.web.Application(urls, **settings.TORNADO)
    http_server = tornado.httpserver.HTTPServer(app, xheaders=True)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
