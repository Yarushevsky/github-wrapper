"""
Description
"""

import functools

from jsonschema import Draft4Validator

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"


class Validator(object):
    def __init__(self, schema):
        self.validator = Draft4Validator
        self.draft = self.validator(schema)

    def validation(self, input_json):
        """
        Validation of json data

        :param input_json: json to validate
        :return:
        """
        self.draft.validate(input_json)

    def validation_iter(self, input_json):
        """
        Validate all values

        :param input_json: json to validate
        :return: generator of errors
        """
        return self.draft.iter_errors(input_json)


def validate(self, function, input_schema, data, *args, **kwargs):
    """
    Json validation

    :param self: instance of handler
    :param function: function to process request in handler
    :param input_schema: json schema
    :param data:
    :param args:
    :param kwargs:
    :return:
    """
    validator = Validator(input_schema)
    errors = validator.validation_iter(data)
    errors_message = [{"path": '.'.join([str(path) for path in error.path]), "message": error.message} for error in
                      errors]
    if errors_message:
        self.send_response(
                errors=errors_message,
                code=409,
                message="Entered data is not valid"
        )
    else:
        return function(self, *args, **kwargs)


def set_types(data, types=None):
    """
    Set types of cls in types to data

    :param data: dict of data
    :param types: dict of types
    :return:
    """
    if types is not None:
        for key, value in types.items():
            try:
                if key not in data:
                    continue
                if isinstance(value, dict):
                    list(data[key])
                    for i in range(len(data[key])):
                        data[key][i] = value['item'](data[key][i])
                else:
                    if isinstance(data[key], list):
                        continue
                    data[key] = value(data[key])
            except ValueError:
                continue


def json_validate_get_params(input_schema, types=None):
    """
    Decorator of request method to add json validation of parameters GET request

    Pattern of types:
    {
        <name>: <type_cls>,         : to set type to value with key <name>
        <name>: {                   : to set type to each item of list
            item: <type_cls>
        }
    }

    :param input_schema: json schema
    :param types: type
    :return:
    """

    def decorator(function):
        @functools.wraps(function)
        def wrapper(self, *args, **kwargs):
            data = self.query_arguments_as_json()
            set_types(data, types)
            return validate(self, function, input_schema, data, *args, **kwargs)

        return wrapper

    return decorator
