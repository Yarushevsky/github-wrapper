"""
Description
"""

import functools

__author__ = 'O.Yarushevskyi'
__credits__ = ["Oleksandr Yarushevsky"]

__license__ = ""
__version__ = "1"
__email__ = "olexand777@gmail.com"
__status__ = "Production"


class HandlerError(Exception):
    def __init__(self, message=None, code=None, error_code=None, errors=None):
        """

        :param message:
        :param code:
        :param error_code:
        :param errors:
        """
        self.message = message
        self.code = code
        self.error_code = error_code
        self.errors = errors
        super(HandlerError, self).__init__()


def wrap_handler_error(function):
    @functools.wraps(function)
    def wrapper(self, *args, **kwargs):
        try:
            return function(self, *args, **kwargs)
        except HandlerError as error:
            self.send_response(
                    code=error.code,
                    message=error.message,
                    error_code=error.error_code,
                    errors=error.errors
            )

    return wrapper
